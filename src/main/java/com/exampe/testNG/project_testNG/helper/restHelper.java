package com.exampe.testNG.project_testNG.helper;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class restHelper
{

	private static OkHttpClient applySslSettings()
	{
		OkHttpClient httpClient = new OkHttpClient();
		try
		{
			KeyManager[] keyManagers = null;
			TrustManager[] trustManagers = null;
			HostnameVerifier hostnameVerifier = null;
			if (true)
			{
				TrustManager trustAll = new X509TrustManager() {
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
					{
					}

					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
					{
					}

					public java.security.cert.X509Certificate[] getAcceptedIssuers()
					{
						return null;
					}
				};
				SSLContext sslContext = SSLContext.getInstance("TLS");
				trustManagers = new TrustManager[] { trustAll };
				hostnameVerifier = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session)
					{
						return true;
					}
				};
			}

			if (keyManagers != null || trustManagers != null)
			{
				SSLContext sslContext = SSLContext.getInstance("TLS");
				sslContext.init(keyManagers, trustManagers, new SecureRandom());
				httpClient.setSslSocketFactory(sslContext.getSocketFactory());
			}
			else
			{
				httpClient.setSslSocketFactory(null);
			}
			httpClient.setHostnameVerifier(hostnameVerifier);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
		return httpClient;
	}

	public static ApiListObject getJsonWithRest(String url) throws Exception
	{
		ApiListObject apiList = null;
		try
		{

			Request.Builder requestBuilder = new Request.Builder().url("https://" + url);
			requestBuilder.header("Accept", "");
			requestBuilder.header("Content-Type", "application/json");
			Request request = requestBuilder.method("GET", null).build();
			OkHttpClient httpClient = applySslSettings();
			Response response = httpClient.newCall(request).execute();
			String content = response.body().string();
			// System.out.print(content);

			// Parse response to json object
			ObjectMapper ObjMap = new ObjectMapper();
			apiList = ObjMap.readValue(content, ApiListObject.class);

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return apiList;
	}

	public static List<ApiObject> compareCategoryInObjects(List<ApiObject> entries)
	{
		List<ApiObject> tempApiObjList = new ArrayList<ApiObject>();
		for (ApiObject apiob : entries)
		{
			if (apiob.getCategory().contains("Authentication & Authorization"))
			{
				tempApiObjList.add(apiob);
			}
		}

		return tempApiObjList;
	}

	public static void printJson(ApiObject apio)
	{
		System.out.println("API:" + apio.getApi() + "\n" + "Description:" + apio.getDescription() + "\n" + "Auth:" + apio.getAuth() + "\n" + "HTTPS:"
				+ apio.getHttps() + "\n" + "Cors:" + apio.getCors() + "\n" + "Link:" + apio.getLink() + "\n" + "Category:" + apio.getCategory());

	}

}

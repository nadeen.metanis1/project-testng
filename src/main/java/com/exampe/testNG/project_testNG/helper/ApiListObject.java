package com.exampe.testNG.project_testNG.helper;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;



public class ApiListObject
{
	@JsonProperty("count")
	private int	 count;
	
	@JsonProperty("entries")
	private List<ApiObject>	entries	= new ArrayList<ApiObject>();

	public int getCount()
	{
		return count;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public List<ApiObject> getEntries()
	{
		return entries;
	}

	public void setEntries(List<ApiObject> entries)
	{
		this.entries = entries;
	}

}

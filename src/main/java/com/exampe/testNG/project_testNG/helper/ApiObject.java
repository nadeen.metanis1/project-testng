package com.exampe.testNG.project_testNG.helper;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiObject
{

	@JsonProperty("API")
	private String	api;

	@JsonProperty("Description")
	private String	description;

	@JsonProperty("Auth")
	private String	auth;

	@JsonProperty("HTTPS")
	private boolean	https;

	@JsonProperty("Cors")
	private String	cors;

	@JsonProperty("Link")
	private String	link;

	@JsonProperty("Category")
	private String	category;

	public ApiObject api(String api)
	{
		this.api = api;
		return this;
	}

	public String getApi()
	{
		return api;
	}

	public void setApi(String api)
	{
		this.api = api;
	}

	public ApiObject description(String description)
	{
		this.description = description;
		return this;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public ApiObject auth(String auth)
	{
		this.auth = auth;
		return this;
	}

	public String getAuth()
	{
		return auth;
	}

	public void setAuth(String auth)
	{
		this.auth = auth;
	}

	public ApiObject https(Boolean https)
	{
		this.https = https;
		return this;
	}

	public boolean getHttps()
	{
		return https;
	}

	public void setHttps(boolean https)
	{
		this.https = https;
	}

	public ApiObject cors(String cors)
	{
		this.cors = cors;
		return this;
	}

	public String getCors()
	{
		return cors;
	}

	public void setCors(String cors)
	{
		this.cors = cors;
	}

	public ApiObject link(String link)
	{
		this.link = link;
		return this;
	}

	public String getLink()
	{
		return link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public ApiObject category(String category)
	{
		this.category = category;
		return this;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public boolean equals(java.lang.Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		ApiObject stdClassObject = (ApiObject) o;
		return Objects.equals(this.api, stdClassObject.api) && Objects.equals(this.auth, stdClassObject.auth)
				&& Objects.equals(this.category, stdClassObject.category) && Objects.equals(this.cors, stdClassObject.cors)
				&& Objects.equals(this.description, stdClassObject.description) && Objects.equals(this.https, stdClassObject.https)
				&& Objects.equals(this.link, stdClassObject.link);

	}

}

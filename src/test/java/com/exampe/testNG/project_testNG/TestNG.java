package com.exampe.testNG.project_testNG;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestNG
{
	public static String getString(InputStream is)
	{
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try
		{
			while ((line = br.readLine()) != null)
			{
				sb.append(line);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	/**
	 * The method will return the search page result in a {@link String} object
	 */
	public static String getSearchContent(String googleSearchQuery) throws Exception
	{
		final String agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36";
		URL url = new URL(googleSearchQuery);
		final URLConnection connection = url.openConnection();
		// System.out.print("host " +url.getHost());
		/**
		 * User-Agent is mandatory otherwise Google will return HTTP response
		 * code: 403
		 */
		connection.setRequestProperty("User-Agent", agent);
		final InputStream stream = connection.getInputStream();
		return getString(stream);
	}

	/**
	 * Parse all links
	 */
	public static List<String> parseLinks(final String html) throws Exception
	{
		List<String> result = new ArrayList<String>();
		Document doc = Jsoup.parse(html);
		Elements results = doc.select("a > h3");
		for (Element link : results)
		{

			Elements parent = link.parent().getAllElements();
			String relHref = parent.attr("href");
			if (relHref.startsWith("/url?q="))
			{
				relHref = relHref.replace("/url?q=", "");
			}
			String[] splittedString = relHref.split("&sa=");
			if (splittedString.length > 1)
			{
				relHref = splittedString[0];
			}
			// System.out.println(relHref);
			result.add(relHref);
		}
		return result;
	}

	public static List<String> getSearchText(final String html) throws Exception
	{
		List<String> result = new ArrayList<String>();
		Document doc = Jsoup.parse(html);
		Elements results = doc.select("a > h3");
		for (Element link : results)
		{

			Elements parent = link.parent().getAllElements();
			String text = parent.text();

			result.add(text);
		}
		return result;
	}

	public static void main(String[] args) throws Exception
	{
		String searchTerm = "java";
		System.out.println("Google Search Bar");
		System.out.println("Searching for: " + searchTerm);
		String query = "https://www.google.com/search?q=" + searchTerm;
		System.out.println(query);
		String page = getSearchContent(query);
		List<String> links = parseLinks(page);
		System.out.println();
		System.out.println("Results:");
		// Print only search result links
		for (int i = 0; i < links.size(); i++)
		{
			System.out.println(links.get(i));
		}
		// Verify first search result contains the word “Java”
		List<String> texts = getSearchText(page);
		if (texts.get(0).contains("java"))
			System.out.println("first search result contains the word “Java” ,First search:" + texts.get(0));

	}

}

package com.exampe.testNG.project_testNG;

import java.util.List;

import com.exampe.testNG.project_testNG.helper.ApiListObject;
import com.exampe.testNG.project_testNG.helper.ApiObject;
import com.exampe.testNG.project_testNG.helper.restHelper;

public class RestMain
{

	public static void main(String[] args) throws Exception
	{
		ApiListObject apiListObj = restHelper.getJsonWithRest("api.publicapis.org/entries");
		List<ApiObject> categoryListApi = restHelper.compareCategoryInObjects(apiListObj.getEntries());
		
		System.out.println("Counter of Objects with property “Category: Authentication & Authorization” is: "+ categoryListApi.size());
		System.out.println("Objects with property “Category: Authentication & Authorization” are: ");
		for(ApiObject listOb: categoryListApi) {
			
			restHelper.printJson(listOb);
			System.out.println("--------------");
		}
	}
}
